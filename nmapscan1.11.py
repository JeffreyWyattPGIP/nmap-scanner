# This Python script scans for IPv4 vulnerabilities using the nmap scripting engine and documents the results
# Python 3 and nmap must be previously installed
# The nmap and script directories must be included in the %path% variable
# The network scan will be limited to classful addressing and private networks

# Import statements
import os
import socket

# Welcome message
print("Welcome to our python nmap scanner!")

# Get IP address by opening a dummy socket and extracting the getsockname()[0] value
dummy_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
dummy_socket.connect(("8.8.8.8", 80))
ip_address = dummy_socket.getsockname()[0]
dummy_socket.close()

# Initialize subnetMask variable
# Defaults to empty string for a localhost scan
subnet_mask = ""

# Initialize choice variable
choice = None
# Prompt for scan type choice
# Valid entries are '1', '2', or '3'
while choice != 1 or choice != 2 or choice != 3:
    try:
        choice = int(input("What would you like to scan? (1,2,3)"
                           "\n1: Localhost scan"
                           "\n2: Network scan"
                           "\n3: Quit\n"))
    # Invalid entries generate a ValueError
    except ValueError:
        print("The valid input options are 1, 2, and 3.")
    if choice == 1:
        # Local IP address already targeted
        break
    elif choice == 2:
        # Get network IP address through local ipAddress value
        # Isolate the first octet and store  subnet mask
        first_octet = int(ip_address.split(".")[0])
        second_octet = int(ip_address.split(".")[1])
        third_octet = int(ip_address.split(".")[2])
        if first_octet == 10:
            subnet_mask = "/8"
            ip_address = str(first_octet) + ".0.0.0"
        elif first_octet == 172 and 16 >= second_octet <= 31:
            subnet_mask = "/16"
            ip_address = str(first_octet) + str(second_octet) + ".0.0"
        elif first_octet == 192 and second_octet == 168:
            subnet_mask = "/24"
            ip_address = str(first_octet) + "." + str(second_octet) + "." + str(third_octet) + ".0"
        else:
            # Any other subnet addresses is public and invalid
            print("The subnet is not private and may not be scanned without proper authorization.")
        break
    elif choice == 3:
        print("Thank you for using our tool!")
        # Exit Python
        exit()

# Designate file to save output
save_file = ' > nmap_vulnerability_scan.txt'

# Set nmap command string
nmap_command_string = 'nmap -sV --script vulners ' + ip_address + subnet_mask + save_file

# Issue command for scan and save results to nmap_vulnerability_scan.txt
# in current working directory
os.system(nmap_command_string)

# Initialize list to store text file data
nmap_scan_data = []

# Import data from nmap_vulnerability_scan.txt
with open('nmap_vulnerability_scan.txt') as text_file:
    # Transfer data from nmap_vulnerability_scan.txt to list
    for line in text_file:
        nmap_scan_data.append(line)

# Close file handle
text_file.close()

# Parse information for https addresses
# Exclude nmap.org addresses
# Remove "|_    	" and "\n"
parsed_nmap_scan_data = []
for line in nmap_scan_data:
    if line.__contains__('https') and not line.__contains__("nmap") or line.__contains__("scan report"):
        parsed_nmap_scan_data.append(line.lstrip("|_    	").rstrip('\n'))

# No vulnerabilities discovered for local host scan
if not len(parsed_nmap_scan_data) > 1:
    parsed_nmap_scan_data.append("No known vulnerabilities were detected")

# Print results to screen
for line in parsed_nmap_scan_data:
    print(line)

# File handle for nmap_vulnerability_scan.txt
with open('nmap_vulnerability_scan.txt', 'w') as text_file:
    # Write parsed data to text file
    for line in parsed_nmap_scan_data:
        # write each item on a new line
        text_file.write(line + '\n')

# Close file handle
text_file.close()

# Exit statement
print("Thank you for using our tool!")
